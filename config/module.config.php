<?php

return [
    'service_manager' => [
        'factories' => [
            'SlmQueueRmq\Options\RmqOptions'  => 'SlmQueueRmq\Factory\RmqOptionsFactory',
            'SlmQueueRmq\Service\StompClient' => 'SlmQueueRmq\Factory\StompClientFactory',
            'SlmQueueRmq\Worker\RmqWorker'    => 'SlmQueue\Factory\WorkerFactory'
        ]
    ],

    'controllers' => [
        'factories' => [
            'SlmQueueRmq\Controller\RmqWorkerController' => 'SlmQueueRmq\Factory\RmqWorkerControllerFactory',
        ],
    ],

    'console'   => [
        'router' => [
            'routes' => [
                'slm-queue-rmq-worker' => [
                    'type'    => 'Simple',
                    'options' => [
                        'route'    => 'queue rmq <queue> [--timeout=]',
                        'defaults' => [
                            'controller' => 'SlmQueueRmq\Controller\RmqWorkerController',
                            'action'     => 'process'
                        ],
                    ],
                ],
            ],
        ],
    ],

    'slm_queue' => [
        'rabbit_mq' => [
            'broker_url' => '',
        ],
        'worker_strategies' => [
            'queues' => [
                'default' => [
                    'SlmQueueRmq\Strategy\SubscribeStrategy',
                ],
            ]
        ],
        'strategy_manager' => [
            'invokables' => [
                'SlmQueueRmq\Strategy\SubscribeStrategy' => 'SlmQueueRmq\Strategy\SubscribeStrategy',
            ],
        ],
    ]
];
