<?php

namespace SlmQueueRmq\Strategy;

use SlmQueue\Strategy\AbstractStrategy;
use SlmQueue\Worker\WorkerEvent;
use SlmQueueRmq\Queue\RmqQueueInterface;
use SlmQueueRmq\Worker\Exception\InvalidQueueException;
use Zend\EventManager\EventManagerInterface;
use SlmQueueRmq\Queue\RmqQueue;

class SubscribeStrategy extends AbstractStrategy
{
    /**
     * {@inheritdoc}
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(
            WorkerEvent::EVENT_BOOTSTRAP,
            [$this, 'connectAndSubscribe'],
            $priority
        );
    }

    /**
     * @param WorkerEvent $event
     */
    public function connectAndSubscribe(WorkerEvent $event)
    {
        /**
         * @var $queue RmqQueue;
         */
        $queue = $event->getQueue();
        if (!$queue instanceof RmqQueueInterface) {
            throw new InvalidQueueException;
        }

        $queue->ensureConnection();
        $queue->subscribe();
    }
}