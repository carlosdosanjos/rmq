<?php

namespace SlmQueueRmq\Worker\Exception;

use SlmQueueRmq\Exception\ExceptionInterface;
use Zend\File\Exception\InvalidArgumentException;


class InvalidQueueException extends InvalidArgumentException implements ExceptionInterface
{

}