<?php

namespace SlmQueueRmq\Worker;

use SlmQueue\Job\JobInterface;
use SlmQueue\Queue\QueueInterface;
use SlmQueue\Worker\AbstractWorker;
use SlmQueue\Worker\WorkerEvent;
use SlmQueueRmq\Queue\RmqQueueInterface;
use Stomp\Transport\Frame;

class RmqWorker extends AbstractWorker
{

    /**
     * Process a job that comes from the given queue
     * @todo implement verbosity flag
     * @param  JobInterface $job
     * @param  QueueInterface $queue
     * @return int Status of the job
     */
    public function processJob(JobInterface $job, QueueInterface $queue)
    {
        if(! $queue instanceof RmqQueueInterface) {
            return WorkerEvent::JOB_STATUS_UNKNOWN;
        }

        try {
            $job->execute();
        }
        catch(\Exception $exception) {
            echo $exception->getMessage();
            return WorkerEvent::JOB_STATUS_FAILURE_RECOVERABLE;
        }
    }
}