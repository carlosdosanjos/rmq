<?php

namespace SlmQueueRmq\Queue;

use SlmQueue\Queue\QueueInterface;

interface RmqQueueInterface extends QueueInterface
{
    const DELAY      = 'RMQ_SCHEDULED_DELAY';
    const PERIOD     = 'RMQ_SCHEDULED_PERIOD';
    const REPEAT     = 'RMQ_SCHEDULED_REPEAT';
    const CRON       = 'RMQ_SCHEDULED_CRON';
    const PERSISTENT = 'persistent';
}