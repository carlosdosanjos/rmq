<?php

namespace SlmQueueRmq\Queue;

use SlmQueue\Job\JobInterface;
use SlmQueue\Job\JobPluginManager;
use SlmQueue\Queue\AbstractQueue;
use SlmQueueRmq\Options\QueueOptions;
use Stomp\Client as StompClient;
use Stomp\Exception\StompException;
use Stomp\StatefulStomp as StompInstance;
use Stomp\States\Meta\SubscriptionList;
use Stomp\Transport\Frame;

class RmqQueue extends AbstractQueue implements RmqQueueInterface
{

    protected $stompClient;

    protected $options;

    protected $rmqOptions;

    protected $stompInstance;

    public function __construct(StompClient $stompClient, QueueOptions $options, $name, JobPluginManager $jobPluginManager)
    {
        $this->stompClient = $stompClient;
        $this->options = $options;
        $this->stompInstance = new StompInstance($this->stompClient);
        parent::__construct($name, $jobPluginManager);
    }

    /**
     * Push a new job into the queue
     *
     * @param  JobInterface $job
     * @param  array $options
     * @return void
     */
    public function push(JobInterface $job, array $options = array())
    {
        $allowed = [self::DELAY, self::PERIOD, self::REPEAT, self::CRON, self::PERSISTENT];
        $options = array_intersect_key($options, array_flip($allowed));

        if (array_key_exists(self::PERSISTENT, $options) && true === (bool) $options[self::PERSISTENT]) {
            $options[self::PERSISTENT] = 'true';
        } elseif (array_key_exists(self::PERSISTENT, $options)) {
            unset($options[self::PERSISTENT]);
        }

        $this->ensureConnection();

        $this->stompClient->send(
            $this->options->getDestination(),
            $this->serializeJob($job),
            $options
        );
    }

    /**
     * Pop a job from the queue
     * @throws StompException $e
     * @param  array $options
     * @return JobInterface|null
     */
    public function pop(array $options = array())
    {
        $options += $this->options->getClientId() ? [self::PERSISTENT => true] : [];
        if (array_key_exists('timeout', $options)) {
            $this->stompClient->getConnection()->setReadTimeout((int) $options['timeout'], 0);
        }

        try {
            /**
             * @var $frame Frame;
             */
            $frame = $this->stompInstance->read();
        } catch (StompException $e) {
            // Silently ignore unreadable sockets because this happens when you quit the worker
            if ($e->getMessage() === 'Check failed to determine if the socket is readable') {
                return;
            }

            throw $e;
        }

        if ($frame === false) {
            return null;
        }

        return $this->unserializeJob($frame->getBody());
    }

    /**
     * Delete a job from the queue
     *
     * @param  JobInterface $job
     * @return void
     */
    public function delete(JobInterface $job)
    {
        $this->stompInstance->ack($job);
    }

    public function ensureConnection()
    {
        if (!$this->stompClient->isConnected()) {
            $this->stompClient->connect();
        }
    }

    public function subscribe()
    {
        $destination   = $this->options->getDestination();
        /**
         * @var $subscriptions SubscriptionList
         */
        $subscriptions = $this->stompInstance->getSubscriptions();

        if (!in_array($destination, $subscriptions->getIterator()->getArrayCopy())) {
            $this->stompInstance->subscribe($destination);
        }
    }
}