<?php
namespace SlmQueueRmq\Factory;

use SlmQueueRmq\Options\RmqOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RmqOptionsFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        return new RmqOptions($config['slm_queue']['rabbit_mq']);
    }
}