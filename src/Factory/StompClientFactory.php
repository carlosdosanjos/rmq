<?php

namespace SlmQueueRmq\Factory;

use SlmQueueRmq\Options\RmqOptions;
use Stomp\Client as StompClient;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class StompClientFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /**
         * @var $options RmqOptions
         */
        $options = $serviceLocator->get('SlmQueueRmq\Options\RmqOptions');
        $stompClient = new StompClient($options->getBrokerUrl());
        $stompClient->setLogin($options->getUserName(), $options->getPassword());
        $stompClient->setVhostname($options->getVHost());

        return $stompClient;
    }
}