<?php

namespace SlmQueueRmq\Factory;

use SlmQueueRmq\Queue\RmqQueue;
use SlmQueueRmq\Options\QueueOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use SlmQueueRmq\Service\StompClient;
use SlmQueue\Job\JobPluginManager;


class RmqQueueFactory implements FactoryInterface
{

    /**
     * {@inheritdoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator, $name = '', $requestedName = '')
    {
        $parentLocator = $serviceLocator->getServiceLocator();

        $config = $parentLocator->get('Config')['slm_queue']['queues'];


        $queueConfig = array_key_exists($requestedName, $config) ? $config[$requestedName] : [];
        $queueOptions = new QueueOptions($queueConfig);


        /**
         * @var $stompClient StompClient
         */
        $stompClient = $parentLocator->get(StompClient::class);


        /**
         * @var $jobPluginManager JobPluginManager
         */
        $jobPluginManager = $parentLocator->get(JobPluginManager::class);

        if($queueOptions->getClientId() !== null) {
            $stompClient->setClientId($queueOptions->getClientId());
        }

        return new RmqQueue($stompClient, $queueOptions, $requestedName, $jobPluginManager);
    }
}