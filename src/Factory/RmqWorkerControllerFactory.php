<?php


namespace SlmQueueRmq\Factory;


use SlmQueueRmq\Controller\RmqWorkerController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RmqWorkerControllerFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $worker = $serviceLocator->getServiceLocator()->get('SlmQueueRmq\Worker\RmqWorker');
        $manager = $serviceLocator->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager');

        return new RmqWorkerController($worker, $manager);
    }
}