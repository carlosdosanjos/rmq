<?php

namespace SlmQueueRmq\Options;

use Zend\Stdlib\AbstractOptions;

class RmqOptions extends AbstractOptions
{
    /**
     * @var string
     */
    protected $brokerUrl;

    /**
     * @var string
     */
    protected $userName;

    /**
     * @var string
     */
    protected $vHost;

    /**
     * @var string
     */
    protected $password;

    /**
     * @return string
     */
    public function getBrokerUrl()
    {
        return $this->brokerUrl;
    }

    /**
     * @param string $brokerUrl
     */
    public function setBrokerUrl($brokerUrl)
    {
        $this->brokerUrl = $brokerUrl;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return string
     */
    public function getVHost()
    {
        return $this->vHost;
    }

    /**
     * @param string $vHost
     */
    public function setVHost($vHost)
    {
        $this->vHost = $vHost;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
}