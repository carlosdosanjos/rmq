<?php

namespace SlmQueueRmq\Service;

use Stomp\Client;

class StompClient extends Client
{
    public function __construct($broker)
    {
        parent::__construct($broker);
    }

}