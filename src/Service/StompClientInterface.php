<?php

namespace SlmQueueRmq\Service;

interface StompClientInterface
{
    public function connect($username = 'admin', $password = 'admin');
    public function isConnected();
    public function send($destination, $msg, array $header = array(), $sync = null);

    public function subscribe($destination, $properties = [], $sync = null);
    public function unsubscribe($destination, $properties = [], $sync = null);
    public function begin($transactionId = null, $sync = null);
    public function commit($commit = null, $sync = null);
    public function abort($transactionId = null, $sync = null);
    public function ack($message, $transactionId = null);
    public function disconnect();
    public function readFrame();
    public function setReadTimeout($seconds, $milliseconds = 0);

    public function getSubscriptions();
}