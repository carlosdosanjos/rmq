<?php

namespace SlmQueueRmq\Service;


interface AMQPClientInterface
{
    public function connect($url  = 'localhost', $port = 5672, $username = 'admin', $password = 'admin');

    public function getSubscriptions();

}