<?php
namespace SlmQueueRmq;

use Zend\ModuleManager\Feature;
use Zend\Console\Adapter\AdapterInterface;


/**
 * SlmQueueRmq
 */
class Module implements
    Feature\ConfigProviderInterface,
    Feature\ConsoleBannerProviderInterface,
    Feature\ConsoleUsageProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * {@inheritDoc}
     */
    public function getConsoleBanner(AdapterInterface $console)
    {
        return 'SlmQueueRmq';
    }

    /**
     * {@inheritDoc}
     */
    public function getConsoleUsage(AdapterInterface $console)
    {
        return array(
            'queue rmq <queue> [--timeout=]' => 'Process jobs with Rabbit MQ',

            array('<queue>', 'Queue\'s name ("destination") to process'),
            array('--timeout=', 'Timeout (in seconds) to wait for a job to arrive')
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getModuleDependencies()
    {
        return array('SlmQueue');
    }
}
